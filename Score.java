package jap_c4_s1_exceptionhandling_pc2;

import java.util.InputMismatchException;

public class Score {

	public void display(String[] names, String[] scores) {
		int highest = 0, index = 0;
		float sum = 0;
		try {
			for (int i = 0; i < names.length; i++) {
				int num = Integer.parseInt(scores[i]);
				sum += num;
				if (num > highest) {
					highest = num;
					index = i;
				}
			}
			System.out.println("The average score of 10 school is :" + (sum / names.length));
			System.out.println("The Highest Scorer in the Quiz is " + names[index] + " with a score of :" + highest);
		}
		 catch (InputMismatchException e)
		{
			e.printStackTrace();
		}
		 catch (ArrayIndexOutOfBoundsException e) 
		{
			e.printStackTrace();
		}
		 catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		 catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		String[] nameOfSchools = { "DAV", "RSK", "Treamis", "Candor", "oak", "UAV", "BGCS", "BCBS", "Baldwin", "NPS" };
		String[] scores = { "86", "78", "55", "6", "44", "33", "82", "77", "8", "99" };
		Score obj = new Score();
		obj.display(nameOfSchools, scores);

	}

}
